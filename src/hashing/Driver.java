/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashing;

import java.util.HashSet;
import java.util.Random;

/**
 *
 * @author vhe17qgu
 */
public class Driver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Testing our ArrayHashTable
        long durationAvgAHT = 0;
        long durationAvgHS = 0;
        for (int i = 0; i < 50; i++) {
            ArrayHashTable AHT = new ArrayHashTable();
            int[] numbersAHT = new int[50000];
            Random r1 = new Random();
            //Creating random numbers for AHT
            for (int j = 0; j < numbersAHT.length; j++) {
                numbersAHT[j] = Math.abs(r1.nextInt());
            }
            
            //Timing Experiments
            long startTime = System.nanoTime();
            for (int j = 0; j < numbersAHT.length; j++) {
                AHT.add(numbersAHT[j]);
            }
           
            for (int j = 0; j < numbersAHT.length; j++) {
                AHT.remove(numbersAHT[j]);
            }
            long endTime = System.nanoTime();
            long duration = (endTime - startTime);
            //Discarding the first 20 trials to counteract JVM warmup 
            if (i >= 20) {
                durationAvgAHT += duration / 1000;
                System.out.println("Time to add and remove "
                        + numbersAHT.length + " Integers from ArrayHashTable (Microseconds): " + duration / 1000);
            }
        }
        System.out.println("Average Duration over 30 trials: " + durationAvgAHT / 30);

        //Testing Java HashSet
        for (int i = 0; i < 50; i++) {
            HashSet HS = new HashSet();
            int[] numbersHS = new int[50000];
            Random r2 = new Random();
            //Generating Random Numbers
            for (int j = 0; j < numbersHS.length; j++) {
                numbersHS[j] = Math.abs(r2.nextInt());
            }
            long startTime = System.nanoTime();
            for (int j = 0; j < numbersHS.length; j++) {
                HS.add(numbersHS[j]);
            }
            for (int j = 0; j < numbersHS.length; j++) {
                HS.remove(numbersHS[j]);
            }
            long endTime = System.nanoTime();
            long duration = (endTime - startTime);
            //Discarding the first 20 trials to counteract JVM warmup 
            if (i >= 20) {
                durationAvgHS += duration / 1000;
                System.out.println("Time to add and remove " 
                        + numbersHS.length + " Integers from HashSet (Microseconds): " + duration / 1000);
            }
            
        }
        System.out.println("Average Duration over 30 trials: " + durationAvgHS / 30);
    }
    }
