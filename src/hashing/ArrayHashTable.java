/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashing;

import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author vhe17qgu
 */
public class ArrayHashTable extends HashTable {

    int chainSize;
    Object table[][];
    int capacity;
    int[] counts;

    //CONSTRUCTOR
    public ArrayHashTable() {
        chainSize = 5;
        capacity = 10;
        table = new Object[capacity][];
        counts = new int[capacity];
        Arrays.fill(counts, 0);
    }

    @Override
    boolean add(Object obj) {
        //Get index of the required bucket
        int index = obj.hashCode() % capacity;
        //If the bucket contains no entries
        if (table[index] == null) {
            table[index] = new Object[chainSize];
            table[index][0] = obj;
            counts[index]++;
            return true;
        }

        if (contains(obj)) {
            //System.out.println("ERROR: OBJECT ALREADY PRESENT");
            return false;
        } else {
            //If the number of items in the bucket is equal to the size of the bucket
            if (counts[index] == table[index].length) {
                int newSize = table[index].length * 2;
                Object[] expanded = Arrays.copyOf(table[index], newSize);
                table[index] = expanded;
            }
            //Loop through to add to the first null value
            for (int i = 0; i < table[index].length; i++) {
                if (table[index][i] == null) {
                    table[index][i] = obj;
                    counts[index]++;
                    return true;
                }
            }
        }
        //System.out.println("ERROR: COULD NOT ADD");
        return false;
    }

@Override
    boolean contains(Object obj) {
        int index = obj.hashCode() % capacity;
        if (table[index] != null) {
            for (Object o : table[index]) {
                if (obj.equals(o)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    boolean remove(Object obj) {
        int index = obj.hashCode() % capacity;
        //If it contains the object
        boolean removed = false;
        if (contains(obj)) {
            //Loop through the table index chain
            for (int i = 0; i < table[index].length; i++) {
                //If the object is equal to the object stored at table index i 
                if (obj.equals(table[index][i])) {
                    //remove the object at table index i
                    table[index][i] = null;
                    removed = true;
                    //reduce count of objects
                    counts[index]--;
                    //For all objects in the chain
                    for (int j = i + 1; j < table[index].length; j++) {
                        //If the object after the one we have just removed is not null
                        if (table[index][j] != null) {
                            //set new null to the object above
                            table[index][j - 1] = table[index][j];
                            //remove double object
                            table[index][j] = null;
                        }
                    }
                    //If the size of the chain is not 0 and if number of elements 
                    if (counts[index] >= chainSize && counts[index] <= table[index].length / 2) {
                        int newSize = table[index].length / 2;
                        Object[] contracted = Arrays.copyOf(table[index], newSize);
                        table[index] = contracted;
                        return true;
                    }
                    if (counts[index] == 0) {
                        table[index] = null;
                        return true;
                    }

                    if (removed) {
                        return true;
                    }
                }
            }
        } else {
            //System.out.println("OBJECT COULD NOT BE FOUND");
            return false;
        }
        System.out.println("A FATAL ERROR HAS OCCURED");
        return false;
    }

//    
//        @Override
//    boolean contains(Object obj) {
//        int index = obj.hashCode() % capacity;
//        
//
//        int objVal = (int) obj;
//        if (table[index] != null) {
//            int left = 0, right = table[index].length - 1;
//            while(left < right){
//                int mid = left + (right - 1) /2;
//                
//                if((int)table[index][mid] == objVal){
//                    return true;
//                }
//                
//                if((int) table[index][mid] < objVal){
//                    left = mid + 1;
//                }
//                
//                else
//                    right = mid - 1;
//            }            
//        }
//        return false;
//    }
}
